﻿using Microsoft.AspNetCore.Mvc;
using ProjectsTasks.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using ProjectsTasks.Common.DTOs.Team;
using System.Threading.Tasks;

namespace ProjectsTasks.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;

        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> GetAll()
        {
            return (await _teamsService.GetAll()).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            return await _teamsService.Get(id);
        }

        [HttpGet("withUsersOlderThan/{minYearsNumber}")]
        public async Task<ActionResult<ICollection<TeamUsersDTO>>> GetTeamUsersOlderThan(int minYearsNumber)
        {
            return (await _teamsService.GetTeamUsersOlderThan(minYearsNumber)).ToList();
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] NewTeamDTO newTeam)
        {
            var createdTeam = await _teamsService.Add(newTeam);

            return Created($"teams/{createdTeam.Id}", createdTeam);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateTeamDTO newTeam)
        {
            newTeam.Id = id;
            await _teamsService.Update(newTeam);
                        
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _teamsService.Delete(id);

            return NoContent();
        }
    }
}
