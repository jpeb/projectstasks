﻿using Microsoft.AspNetCore.Mvc;
using ProjectsTasks.BLL.Services.Abstract;
using ProjectsTasks.Common.DTOs.Project;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsTasks.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> GetAll()
        {
            return (await _projectsService.GetAll()).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            return await _projectsService.Get(id);
        }

        [HttpGet("tasksNumber/{userId}")]
        public async Task<ActionResult<ICollection<ProjectTasksNumberDTO>>> GetUserProjectsTasksNumber(int userId)
        {
            return (await _projectsService.GetUserProjectsTasksNumber(userId)).ToList();
        }

        [HttpGet("statictics")]
        public async Task<ActionResult<ICollection<ProjectStaticticsDTO>>> GetProjectStatictics()
        {
            return (await _projectsService.GetProjectStatictics(20, 3)).ToList();
        }
        

       [HttpPost]
        public async Task<ActionResult> Add([FromBody] NewProjectDTO newProject)
        {
            var createdProject = await _projectsService.Add(newProject);

            return Created($"projects/{createdProject.Id}", createdProject);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateProjectDTO newProject)
        {
            newProject.Id = id;
            await _projectsService.Update(newProject);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _projectsService.Delete(id);

            return NoContent();
        }
    }
}
