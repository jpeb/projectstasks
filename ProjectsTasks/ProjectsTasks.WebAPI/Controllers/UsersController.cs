﻿using Microsoft.AspNetCore.Mvc;
using ProjectsTasks.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using ProjectsTasks.Common.DTOs.User;
using System.Threading.Tasks;

namespace ProjectsTasks.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> GetAll()
        {
            return (await _usersService.GetAll()).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> Get(int id)
        {
            return await _usersService.Get(id);
        }
        
        [HttpGet("withTasks")]
        public async Task<ActionResult<ICollection<UserTasksDTO>>> GetUsersWithTasks()
        {
            return (await _usersService.GetUsersWithTasks()).ToList();
        }
        

        [HttpGet("{id}/taskStatictics")]
        public async Task<ActionResult<UserTaskStaticticsDTO>> GetUserTaskStatictics(int id)
        {
            return await _usersService.GetUserTaskStatictics(id);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] NewUserDTO newUser)
        {
            var createdUser = await _usersService.Add(newUser);

            return Created($"users/{createdUser.Id}", createdUser);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateUserDTO newUser)
        {
            newUser.Id = id;
            await _usersService.Update(newUser);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _usersService.Delete(id);

            return NoContent();
        }
    }
}
