﻿using Microsoft.AspNetCore.Mvc;
using ProjectsTasks.BLL.Services.Abstract;
using System.Collections.Generic;
using System.Linq;
using ProjectsTasks.Common.DTOs.Task;
using System.Threading.Tasks;

namespace ProjectsTasks.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TaskDTO>>> GetAll()
        {
            return (await _tasksService.GetAll()).ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> Get(int id)
        {
            return await _tasksService.Get(id);
        }

        [HttpGet("assignedToUser/{userId}")]
        public async Task<ActionResult<ICollection<TaskDTO>>> GetUserTasksWithNameLessThen(int userId, [FromQuery]int? maxTaskNameLength)
        {
            maxTaskNameLength ??= int.MaxValue;

            return (await _tasksService.GetUserTasksWithNameLessThen(userId, maxTaskNameLength.Value)).ToList();
        }

        [HttpGet("finishedInYear/{finishedYear}")]
        public async Task<ActionResult<ICollection<TaskNameDTO>>> GetUserTasksFinishedInYear(int finishedYear, [FromQuery] int userId)
        {
            return (await _tasksService.GetUserTasksFinishedInYear(userId, finishedYear)).ToList();
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] NewTaskDTO newTask)
        {
            var createdTask = await _tasksService.Add(newTask);

            return Created($"tasks/{createdTask.Id}", createdTask);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateTaskDTO newTask)
        {
            newTask.Id = id;
            await _tasksService.Update(newTask);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _tasksService.Delete(id);

            return NoContent();
        }
    }
}
