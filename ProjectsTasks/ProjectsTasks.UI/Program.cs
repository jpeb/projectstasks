﻿using ProjectsTasks.UI.Configuration;
using ProjectsTasks.UI.Interfaces;
using ProjectsTasks.UI.Menu;
using ProjectsTasks.UI.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectsTasks.UI
{
    internal class Program
    {
        private const string Title = @"
  _____         _        
 |_   _|_ _ ___| | _____ 
   | |/ _` / __| |/ / __|
   | | (_| \__ \   <\__ \
   |_|\__,_|___/_|\_\___/
                         
  Use arrows and Enter for navigation...                                                                                          
";

        private static async Task Main(string[] args)
        {
            Console.Title = "ProjectsTasks.UI";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiUris.BaseAddress)
            };

            IProjectsService projectsService = new ProjectsService(client);

            var app = new AppController(
                projectsService
            );

            List<MenuOption> options = new()
            {
                new MenuOption("Query 1 - Кол-во тасков у проекта пользователя", app.Query1),
                new MenuOption("Query 2 - Список тасков пользователя с name < X", app.Query2),
                new MenuOption("Query 3 - Список выполненых тасков пользователя за X год", app.Query3),
                new MenuOption("Query 4 - Команды пользователей с участниками старше X лет", app.Query4),
                new MenuOption("Query 5 - Список пользователей и их таски", app.Query5),
                new MenuOption("Query 6 - Информация о тасках пользователя", app.Query6),
                new MenuOption("Query 7 - Информация о тасках проектов", app.Query7),
                new MenuOption("Пометить случайную задачу как выполненную(с задержкой)", app.StartDeferredTask, false),
                new MenuOption("Exit", () => 
                {
                    app.Dispose();
                    Environment.Exit(0);
                    return Task.CompletedTask;
                })
            };

            var menu = new ConsoleMenu(options, Title)
            {
                PrimaryColor = (ConsoleColor.White, ConsoleColor.Black),
                TitleColor = (ConsoleColor.Red, ConsoleColor.Black),
                SelectedColor = (ConsoleColor.Black, ConsoleColor.DarkGreen)
            };

            await menu.RunMenu();
        }
    }
}
