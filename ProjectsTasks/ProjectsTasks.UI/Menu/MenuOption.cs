﻿using System;
using System.Threading.Tasks;

namespace ProjectsTasks.UI.Menu
{
    public class MenuOption
    {
        public MenuOption(string name, Func<Task> action, bool await = true)
        {
            Name = name;
            Action = action;
            Await = await;
        }

        public string Name { get; }
        public bool Await { get; }
        public Func<Task> Action { get; }
    }
}
