﻿namespace ProjectsTasks.UI.Configuration
{
    public static class ApiUris
    {
        public const string BaseAddress = "https://localhost:5001/api/";

        public const string Projects = "projects";
        public const string Tasks = "tasks";
        public const string Teams = "teams";
        public const string Users = "users";

        // Query 1
        public static string UserProjectsTasksNumber(int userId) 
            => $"projects/tasksNumber/{userId}"; 

        // Query 2
        public static string UserTasksWithNameLessThen(int userId, int maxTaskNameLength)
            => $"tasks/assignedToUser/{userId}?maxTaskNameLength={maxTaskNameLength}";

        // Query 3
        public static string UserTasksFinishedInYear(int finishedYear, int userId) 
            => $"tasks/finishedInYear/{finishedYear}?userId={userId}";

        // Query 4
        public static string TeamUsersOlderThan(int minYearsNumber) 
            => $"teams/withUsersOlderThan/{minYearsNumber}";

        // Query 5
        public static string UsersWithTasks() 
            => $"users/withTasks";

        // Query 6
        public static string UserTaskStatictics(int userId) 
            => $"users/{userId}/taskStatictics";

        // Query 7
        public static string ProjectStatictics() 
            => $"projects/statictics";
    }
}
