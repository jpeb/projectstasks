﻿namespace ProjectsTasks.Common.DTOs.Team
{
    public class NewTeamDTO
    {
        public string Name { get; set; }
    }
}
