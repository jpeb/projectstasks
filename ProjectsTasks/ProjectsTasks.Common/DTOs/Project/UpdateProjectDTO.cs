﻿using System;
using System.Text.Json.Serialization;

namespace ProjectsTasks.Common.DTOs.Project
{
    public class UpdateProjectDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
    }
}
