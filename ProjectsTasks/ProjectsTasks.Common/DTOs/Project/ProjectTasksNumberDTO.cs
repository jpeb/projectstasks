﻿namespace ProjectsTasks.Common.DTOs.Project
{
    public class ProjectTasksNumberDTO
    {
        public ProjectDTO Project { get; set; }
        public int TasksNumber { get; set; }
    }
}
