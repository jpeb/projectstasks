﻿using ProjectsTasks.Common.DTOs.Project;
using ProjectsTasks.Common.DTOs.Task;

namespace ProjectsTasks.Common.DTOs.User
{
    public class UserTaskStaticticsDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastUserProject { get; set; }
        public int AllUsersLastProjectTasksNumber { get; set; }
        public int UnfinishedUserTasksNumber { get; set; }
        public TaskDTO LongestUserTask { get; set; }
    }
}
