﻿using System;

namespace ProjectsTasks.Common.DTOs.User
{
    public class NewUserDTO
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
