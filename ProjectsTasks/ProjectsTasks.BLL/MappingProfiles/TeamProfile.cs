﻿using AutoMapper;
using ProjectsTasks.Common.DTOs.Team;
using ProjectsTasks.DAL.Entities;
using System.Linq;

namespace ProjectsTasks.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamUsersDTO>().ForMember(t => t.Users, p => p.MapFrom(t => t.Users.OrderByDescending(u => u.CreatedAt)));

            CreateMap<NewTeamDTO, Team>();
            CreateMap<UpdateTeamDTO, Team>();
        }
    }
}
