﻿using AutoMapper;
using ProjectsTasks.Common.DTOs.Task;
using ProjectsTasks.DAL.Entities;

namespace ProjectsTasks.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<Task, TaskNameDTO>();

            CreateMap<NewTaskDTO, Task>();
            CreateMap<UpdateTaskDTO, Task>();
        }
    }
}
