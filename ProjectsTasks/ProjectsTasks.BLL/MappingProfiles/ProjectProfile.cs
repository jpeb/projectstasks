﻿using AutoMapper;
using ProjectsTasks.Common.DTOs.Project;
using ProjectsTasks.DAL.Entities;

namespace ProjectsTasks.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<NewProjectDTO, Project>();
            CreateMap<UpdateProjectDTO, Project>();
        }
    }
}
