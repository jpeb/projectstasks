﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsTasks.BLL.Exceptions;
using ProjectsTasks.BLL.Services.Abstract;
using ProjectsTasks.Common.DTOs.Project;
using ProjectsTasks.Common.DTOs.Task;
using ProjectsTasks.DAL.Entities;
using ProjectsTasks.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectsTasks.BLL.Services
{
    public class ProjectsService : BaseService, IProjectsService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<Project> _projectRepository;

        public ProjectsService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _projectRepository = _context.ProjectRepository;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            var projects = await _projectRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> Get(int projectId)
        {
            var project = await _projectRepository.GetByIdAsync(projectId);

            if (project is null) throw new NotFoundException(nameof(project), projectId);

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> Add(NewProjectDTO project)
        {
            var newProject = _projectRepository.Add(_mapper.Map<Project>(project));
            await _context.SaveChangesAsync();

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public async Task Delete(int projectId)
        {
            var project = await _projectRepository.GetByIdAsync(projectId);

            if (project is null) throw new NotFoundException(nameof(project), projectId);

            _projectRepository.Delete(project);
            await _context.SaveChangesAsync();
        }

        public async Task Update(UpdateProjectDTO newProject)
        {
            var project = await _projectRepository.GetByIdAsync(newProject.Id);

            if (project is null) throw new NotFoundException(nameof(project), newProject.Id);

            _mapper.Map(newProject, project);
            project.UpdatedAt = DateTime.Now;

            _projectRepository.Update(project);
            await _context.SaveChangesAsync();
        }

        // Query 1
        public async Task<ICollection<ProjectTasksNumberDTO>> GetUserProjectsTasksNumber(int userId)
        {
            var prjects = await _projectRepository
                .GetAllAsync(p => p.AuthorId == userId, true, s => s.Include(p => p.Tasks));

            return prjects
                .Select(project => new ProjectTasksNumberDTO
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    TasksNumber = project.Tasks.Count
                })
                .ToList();
        }

        // Query 7
        public async Task<ICollection<ProjectStaticticsDTO>> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting)
        {
            var projects = await _projectRepository.GetAllAsync(null, true,
                    s => s.Include(p => p.Tasks)
                        .Include(p => p.Team)
                        .ThenInclude(t => t.Users));

            return projects
                .Select(project => new ProjectStaticticsDTO
                {
                    Project = _mapper.Map<ProjectDTO>(project),
                    LongestTaskByDescription = project.Tasks.Any()
                               ? _mapper.Map<TaskDTO>(project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2))
                               : null,
                    ShortestTaskByName = project.Tasks.Any()
                               ? _mapper.Map<TaskDTO>(project.Tasks.Aggregate((t1, t2) => t1.Name.Length > t2.Name.Length ? t2 : t1))
                               : null,
                    UsersNumber = project.Description.Length > minProjectDescriptionLengthForCounting
                           || project.Tasks.Count < maxProjectTasksCountForCounting
                           ? project.Team.Users.Count
                           : null
                })
                .ToList();
        }
    }
}
