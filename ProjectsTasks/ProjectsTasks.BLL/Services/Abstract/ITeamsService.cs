﻿using ProjectsTasks.Common.DTOs.Team;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsTasks.BLL.Services.Abstract
{
    public interface ITeamsService
    {
        Task<IEnumerable<TeamDTO>> GetAll();
        Task<TeamDTO> Get(int teamId);
        Task Update(UpdateTeamDTO team);
        Task<TeamDTO> Add(NewTeamDTO team);
        Task Delete(int teamId);
        
        // Query 4
        Task<ICollection<TeamUsersDTO>> GetTeamUsersOlderThan(int minYearsNumber);
    }
}
