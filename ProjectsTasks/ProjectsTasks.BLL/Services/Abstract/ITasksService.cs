﻿using ProjectsTasks.Common.DTOs.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsTasks.BLL.Services.Abstract
{
    public interface ITasksService
    {
        Task<IEnumerable<TaskDTO>> GetAll();
        Task<TaskDTO> Get(int taskId);
        Task Update(UpdateTaskDTO task);
        Task<TaskDTO> Add(NewTaskDTO task);
        Task Delete(int taskId);

        // Query 2
        Task<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);
        // Query 3
        Task<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int userId, int finishedYear);
    }
}
