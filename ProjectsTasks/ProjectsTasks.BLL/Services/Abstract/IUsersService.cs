﻿using ProjectsTasks.Common.DTOs.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsTasks.BLL.Services.Abstract
{
    public interface IUsersService
    {
        Task<IEnumerable<UserDTO>> GetAll();
        Task<UserDTO> Get(int userId);
        Task Update(UpdateUserDTO user);
        Task<UserDTO> Add(NewUserDTO user);
        Task Delete(int userId);

        // Query 5
        Task<ICollection<UserTasksDTO>> GetUsersWithTasks();

        // Query 6
        Task<UserTaskStaticticsDTO> GetUserTaskStatictics(int userId);
    }
}
