﻿using AutoMapper;
using ProjectsTasks.Common.DTOs.User;
using ProjectsTasks.BLL.Services.Abstract;
using ProjectsTasks.DAL.Entities;
using ProjectsTasks.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectsTasks.Common.DTOs.Project;
using ProjectsTasks.Common.DTOs.Task;
using ProjectsTasks.BLL.Exceptions;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectsTasks.BLL.Services
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<User> _userRepository;

        public UsersService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _userRepository = _context.UserRepository;
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            var users = await _userRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public async Task<UserDTO> Get(int userId)
        {
            var user = await _userRepository.GetByIdAsync(userId);

            if (user is null) throw new NotFoundException(nameof(user), userId);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> Add(NewUserDTO user)
        {
            var newUser = _userRepository.Add(_mapper.Map<User>(user));
            await _context.SaveChangesAsync();

            return _mapper.Map<UserDTO>(newUser);
        }

        public async Task Delete(int userId)
        {
            var user = await _userRepository.GetByIdAsync(userId);

            if (user is null) throw new NotFoundException(nameof(user), userId);

            _userRepository.Delete(user);
            await _context.SaveChangesAsync();
        }

        public async Task Update(UpdateUserDTO newUser)
        {
            var user = await _userRepository.GetByIdAsync(newUser.Id);

            if (user is null) throw new NotFoundException(nameof(user), newUser.Id);

            _mapper.Map(newUser, user);
            user.UpdatedAt = DateTime.Now;

            _userRepository.Update(user);
            await _context.SaveChangesAsync();
        }

        // Query  5
        public async Task<ICollection<UserTasksDTO>> GetUsersWithTasks()
        {
            return _mapper.Map<ICollection<UserTasksDTO>>(
                    await _userRepository.GetAllAsync(null, true, s => s.Include(p => p.Tasks)))
                .OrderBy(u => u.FirstName)
                .ToList();
        }

        // Query 6
        public async Task<UserTaskStaticticsDTO> GetUserTaskStatictics(int userId)
        {
            var users = await _userRepository.GetAllAsync(u => u.Id == userId, true, s => s.Include(u => u.Tasks));
            var projects = await _context.ProjectRepository.GetAllAsync(null, true, s => s.Include(u => u.Tasks));

            //var usersTask = _userRepository.GetAllAsync(u => u.Id == userId, true, s => s.Include(u => u.Tasks));
            //var projectsTask = _context.ProjectRepository.GetAllAsync(null, true, s => s.Include(u => u.Tasks));

            //await Task.WhenAll(usersTask, projectsTask);

            //var users = await usersTask;
            //var projects = await projectsTask;

            var result = from user in users
                         let sortedProjects = projects.OrderByDescending(p => p.CreatedAt)
                         let lastProject = sortedProjects.First()
                         let lastUserProject = sortedProjects.FirstOrDefault(p => p.AuthorId == userId)
                         select new UserTaskStaticticsDTO
                         {
                             User = _mapper.Map<UserDTO>(user),
                             LastUserProject = _mapper.Map<ProjectDTO>(lastUserProject),
                             AllUsersLastProjectTasksNumber = lastProject.Tasks.Count,
                             UnfinishedUserTasksNumber = user.Tasks.Count(t => t.State != TaskState.Finished),
                             LongestUserTask = _mapper.Map<TaskDTO>(user.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault())
                         };

            return result.FirstOrDefault();
        }
    }
}
