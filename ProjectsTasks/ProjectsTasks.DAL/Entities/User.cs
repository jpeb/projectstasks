using ProjectsTasks.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectsTasks.DAL.Entities
{
    public class User : BaseEntity
    {
        [Required]
        [MaxLength(300)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(300)]
        public string LastName { get; set; }

        [MaxLength(500)]
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
