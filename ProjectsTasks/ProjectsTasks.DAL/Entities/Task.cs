using ProjectsTasks.DAL.Entities.Abstract;
using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectsTasks.DAL.Entities
{
    public class Task : BaseEntity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime? FinishedAt { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }

        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
