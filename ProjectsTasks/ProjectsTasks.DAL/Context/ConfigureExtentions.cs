﻿using Microsoft.EntityFrameworkCore;
using Task = ProjectsTasks.DAL.Entities.Task;

namespace ProjectsTasks.DAL.Context
{
    public static class ConfigureExtentions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
