﻿using ProjectsTasks.DAL.Entities;
using System;
using System.Threading.Tasks;
using Task = ProjectsTasks.DAL.Entities.Task;

namespace ProjectsTasks.DAL.Repositories.Abstract
{
    public interface IUnitOfWork : IAsyncDisposable
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Task> TaskRepository { get; }
        IRepository<Team> TeamRepository { get; }
        IRepository<User> UserRepository { get; }

        Task<int> SaveChangesAsync();
    }
}
