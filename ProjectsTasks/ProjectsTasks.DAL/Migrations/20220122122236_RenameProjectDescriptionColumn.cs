﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectsTasks.DAL.Migrations
{
    public partial class RenameProjectDescriptionColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Projects",
                newName: "About");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "About",
                table: "Projects",
                newName: "Description");
        }
    }
}
